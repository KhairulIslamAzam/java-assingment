package ProgramOneEvenOdd;

import java.util.ArrayList;
import java.util.Scanner;

public class EvenOdd {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int numberOne, numberTwo;
        System.out.print("Starting Number: ");
        numberOne = sc.nextInt();
        System.out.print("Last Number: ");
        numberTwo = sc.nextInt();

        if(numberOne>=0 && numberTwo>0) {

            int temp;

            if(numberTwo < numberOne) {

                temp = numberOne;
                numberOne = numberTwo;
                numberTwo = temp;
            }

            ckEvenOddNumber(numberOne,numberTwo);
        }
        else {

            System.out.println("You Enter An Invalid Sequence!!!");
        }

        sc.close();
    }

    public static void ckEvenOddNumber(int numberOne, int numberTwo){

        int evenCount = 0,oddCount=0;
        ArrayList<Integer> arrayList = new ArrayList<Integer>();

        for(int i = numberOne; i <= numberTwo; i++){

            if(i == 0){

            }
            else if(i % 2 == 0){
                evenCount++;
                arrayList.add(i);
            }
            else{
                oddCount++;
            }
        }

        if(arrayList.isEmpty()){

            System.out.println("Total even number count: "+evenCount);
            System.out.println("Total odd number count: "+oddCount);
            System.out.println("There is no even number between "+numberOne+" and "+numberTwo);
        }
        else{

            System.out.println("Total even number count: "+evenCount);
            System.out.println("Total odd number count: "+oddCount);
            System.out.println("The even number between "+numberOne+" and "+numberTwo+" are: "+arrayList);
        }
    }
}
