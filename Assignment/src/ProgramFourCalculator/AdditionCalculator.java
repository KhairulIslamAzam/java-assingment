package ProgramFourCalculator;

public class AdditionCalculator extends BasicCalculator {
    private double firstNumber;
    private double secondNumber;

    public AdditionCalculator(double firstNumber, double secondNumber) {
        super(firstNumber, secondNumber);
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
    }

    @Override
    public void displayCalculation() {

        System.out.print("First Number: "+this.firstNumber);
        System.out.print("\nSecond Number: "+this.secondNumber+"\n");
        System.out.println("Additon of " + this.firstNumber + " + " + this.secondNumber +
                " = " + (this.firstNumber + this.secondNumber));;

    }
}