package ProgramFourCalculator;

public class MultiplicationCalculator extends BasicCalculator {

    private double firstNumber;
    private double secondNumber;

    public MultiplicationCalculator(double firstNumber, double secondNumber) {
        super(firstNumber, secondNumber);
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
    }

    @Override
    public void displayCalculation() {

        System.out.print("First Number: "+this.firstNumber);
        System.out.print("\nSecond Number: "+this.secondNumber+"\n");
        System.out.println("Multiplication of " + this.firstNumber + " X " + this.secondNumber +
                " = " + (this.firstNumber * this.secondNumber));

    }
}