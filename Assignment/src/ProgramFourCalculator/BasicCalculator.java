package ProgramFourCalculator;

public class BasicCalculator extends BasicCalculatorAbstraction{

        private double firstNumber;
        private double secondNumber;
        private char chr;

        public BasicCalculator(char chr) {
            this.chr = chr;
        }

        public BasicCalculator(double firstNumber, double secondNumber) {
            this.firstNumber = firstNumber;
            this.secondNumber = secondNumber;
        }

        public char getChr() {
            return this.chr;
        }

        @Override
        public final boolean checkCharecter(char chr){

            if(chr == '+' || chr == '-' || chr == '*' || chr == '/') {
                return true;
            }
            else {
                return false;
            }
        }

        @Override
        public void displayCalculation() {
            //for future use
        }

}
