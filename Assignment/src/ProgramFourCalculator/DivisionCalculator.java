package ProgramFourCalculator;

public class DivisionCalculator extends BasicCalculator {
    private double firstNumber;
    private double secondNumber;

    public DivisionCalculator(double firstNumber, double secondNumber) {
        super(firstNumber, secondNumber);
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
    }

    @Override
    public void displayCalculation() {

        System.out.print("First Number: "+this.firstNumber);
        System.out.print("\nSecond Number: "+this.secondNumber+"\n");
        System.out.println("Division of "+this.firstNumber+" / "+this.secondNumber+
                " = "+String.format("%.3f",(this.firstNumber / this.secondNumber)));

    }
}

