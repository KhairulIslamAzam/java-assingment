package ProgramFourCalculator;

public abstract class BasicCalculatorAbstraction {

    public abstract boolean checkCharecter(char chr);
    public abstract void displayCalculation();

}
