package ProgramFourCalculator;

public class SubtractionCalculator extends BasicCalculator {

    private double firstNumber;
    private double secondNumber;

    public SubtractionCalculator(double firstNumber, double secondNumber) {
        super(firstNumber, secondNumber);
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
    }

    @Override
    public void displayCalculation() {

        System.out.print("First Number: "+this.firstNumber);
        System.out.print("\nSecond Number: "+this.secondNumber+"\n");
        System.out.println("Subtraction of " + this.firstNumber + " - " + this.secondNumber +
                " = " + (this.firstNumber - this.secondNumber));;

    }
}
