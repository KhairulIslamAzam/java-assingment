package ProgramFourCalculator;

import java.util.Scanner;

public class MainMethodDemo {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("First Number: " );
        double firstNumber = sc.nextLong();
        System.out.print("Enter Operator: ");
        char chr = sc.next().charAt(0);
        System.out.print("Second Number: ");
        double secondNumber = sc.nextLong();

        BasicCalculator basicCalAbs = new BasicCalculator (chr);

        if(basicCalAbs.checkCharecter(chr)){

            //basicCal = new BasicCalculatorInheritance(firstNumber,secondNumber);

            if(basicCalAbs.getChr() == '+'){

                AdditionCalculator addCal = new AdditionCalculator(firstNumber,secondNumber);
                addCal.displayCalculation();

            }
            else if(basicCalAbs.getChr() =='-'){

                SubtractionCalculator subCal = new SubtractionCalculator(firstNumber,secondNumber);
                subCal.displayCalculation();
            }
            else if(basicCalAbs.getChr() =='*'){

                MultiplicationCalculator multCal = new MultiplicationCalculator(firstNumber,secondNumber);
                multCal.displayCalculation();
            }
            else if( basicCalAbs.getChr() =='/'){

                DivisionCalculator divCal = new DivisionCalculator(firstNumber,secondNumber);
                divCal.displayCalculation();

            }
        }
        else{

            System.out.println("You Enter a wrong Oparetor or the method still not Implemented");

        }

        sc.close();
    }
}
