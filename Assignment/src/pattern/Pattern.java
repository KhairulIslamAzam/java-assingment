package pattern;

import java.util.Scanner;

public class Pattern {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        byte n = sc.nextByte();

        for (int i = 1; i <= n; i++){
            for(int j = 1; j <= i; j++){

                if(j == n){
                    
                    System.out.print("*");
                    System.out.print("*");
                    System.out.println();

                    for(int k = n -1; k >= 1; k--){

                        for(int l = 1; l <=k; l++){
                            System.out.print("*");
                        }
                        System.out.println();
                    }
                }
                else {
                    System.out.print("*");
                }
            }
            System.out.println();
        }

        sc.close();
    }
}
