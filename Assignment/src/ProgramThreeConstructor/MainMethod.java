package ProgramThreeConstructor;

import java.util.Scanner;

public class MainMethod {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        double height, width, radius;

        System.out.print("Enter Height: ");
        height = sc.nextDouble();
        System.out.print("Enter width: ");
        width = sc.nextDouble();
        System.out.print("Enter Radius: ");
        radius = sc.nextDouble();


        if(radius != 0){

            CircleArea circleArea  = new CircleArea(radius);
            System.out.println("Radius of the circle is: "+circleArea.getRadius());
            System.out.println("-----------------------");
            circleArea.calculateArea();
            System.out.println("-----------------------");

        }
        else {
            System.out.println("If radius is zero then it is just a point not Circle");
        }


        TraingleArea traingleArea = new TraingleArea();
        if(traingleArea.ckWidhtHeight(height,width)){
            traingleArea.setWidth(width);
            traingleArea.setHeight(height);
            System.out.println("-----------------------");
            System.out.println("Height is: "+traingleArea.getHeight());
            System.out.println("Width is: "+traingleArea.getWidth());
            System.out.println("---------------");
            traingleArea.calculateArea();
        }
        else{
            System.out.println("your input is not in correct format");
        }
    }
}
