package ProgramThreeConstructor;

public class TraingleArea extends AreaCalculation {
    private double height;
    private double width;

    public TraingleArea(){
        super();
        //System.out.println("Deafult constructor of base class traingle");
    }

    public double getHeight() {
        return height;
    }

    public boolean ckWidhtHeight(double height , double width){
        if(height > 0 && width > 0 ){
            return true;
        }
        else{
            return false;
        }
    }

    public void setHeight(double height) {

        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {

        this.width = width;

    }

    @Override
    public void calculateArea() {
        System.out.println("Area of Traingle is: "+String.format("%.3f",(this.height+this.width)/2));
    }
}
