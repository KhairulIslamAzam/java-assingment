package ProgramThreeConstructor;

public class AreaCalculation {
    private double height;
    private double width;
    private double radius;

    final static double PI;

    static {
        PI = 3.1415;
    }

    public AreaCalculation(){
        //System.out.println("Default constructor of Parent class");
    }

    public AreaCalculation(double radius) {
        this.radius = radius;
    }

    public AreaCalculation(double height, double width) {
        this.height = height;
        this.width = width;
    }

    public void calculateArea(){
        System.out.println("Area is calculated here");
    }
}
