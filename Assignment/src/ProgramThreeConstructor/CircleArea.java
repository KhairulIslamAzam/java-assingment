package ProgramThreeConstructor;

public class CircleArea extends AreaCalculation {
    private double radius;

    public CircleArea(double radius) {
        super(radius);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }


    @Override
    public void calculateArea() {
        System.out.println("Area of Circle: "+ String.format("%.3f",((this.radius*this.radius)*AreaCalculation.PI)));
    }
}
